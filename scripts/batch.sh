# Usage: bash run.sh [particle] [energy] [events]

# Electrons
# bash run.sh e- "500 MeV" 10000
# bash run.sh e- "5 GeV" 10000
# bash run.sh e- "50 GeV" 10000
# root -b -l -q 'plot.C("g4_e-_500MeV_10000evt")'
# root -b -l -q 'plot.C("g4_e-_5GeV_10000evt")'
# root -b -l -q 'plot.C("g4_e-_50GeV_10000evt")'

# Pions
# bash run.sh pi- "1200 MeV" 10000
# bash run.sh pi- "12 GeV" 10000
# bash run.sh pi- "120 GeV" 10000
# root -b -l -q 'plot.C("g4_pi-_1200MeV_10000evt")'
# root -b -l -q 'plot.C("g4_pi-_12GeV_10000evt")'
# root -b -l -q 'plot.C("g4_pi-_120GeV_10000evt")'

# Protons
# bash run.sh proton "100 GeV" 10000
# bash run.sh proton "1 TeV" 10000
# bash run.sh proton "7 TeV" 10000
# root -b -l -q 'plot.C("g4_proton_100GeV_10000evt")'
# root -b -l -q 'plot.C("g4_proton_1TeV_10000evt")'
# root -b -l -q 'plot.C("g4_proton_7TeV_10000evt")'

# Neutrons - 100 G, 500 G, 1000 G
bash run.sh neutron "100 GeV" 1000
bash run.sh neutron "500 GeV" 1000
bash run.sh neutron "1000 GeV" 1000
root -b -l -q 'plot.C("g4_neutron_100GeV_1000evt")'
root -b -l -q 'plot.C("g4_neutron_500GeV_1000evt")'
root -b -l -q 'plot.C("g4_neutron_1000GeV_1000evt")'
