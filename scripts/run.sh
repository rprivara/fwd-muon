#!/bin/bash

print_usage() {
    echo "Usage: bash run.sh [particle] [energy] [number_of_events]"
}

rebuild_g4() {
    cd ${g4_dir}/build
    make 1> /dev/null
    cd ${script_dir}
}

debug=1

if [ -z "$3" ]; then
    print_usage
    exit 1
fi

if [ ! -z "$4" ];then
    echo -e "\e[31;1mError: Too many parameters.\e[0m"
    exit 1
fi

# Directories
script_dir=`pwd`
out_dir=${script_dir}/../output
g4_dir=${script_dir}/../geant4
g4_exec=${g4_dir}/build/FwdMuon
g4_macro=${g4_dir}/gun.mac

particle=$1
energy=$2
events=$3

# Debug printout
if [ $debug -eq 1 ]; then
    echo -e "\e[30m --------------- DEBUG INFO ---------------\e[0m"
    echo -e "\e[30m Geant4 dir     : ${g4_dir}\e[0m"
    echo -e "\e[30m Output dir     : ${out_dir}\e[0m"
    echo -e "\e[30m Particle       : $particle\e[0m"
    echo -e "\e[30m Energy         : $energy\e[0m"
    echo -e "\e[30m Events         : $events\e[0m"
    echo -e "\e[30m ------------------------------------------\e[0m"
fi

echo "Setting run parameters."
# Set particle type
sed -i "s|/gps/particle.*|/gps/particle $particle|" ${g4_macro}
# Set particle energy
sed -i "s|/gps/ene/mono.*|/gps/ene/mono $energy|" ${g4_macro}
# Set number of events
sed -i "s|/run/beamOn.*|/run/beamOn $events|" ${g4_macro}
# Update G4 macro
rebuild_g4

# Run Geant4 simulation
echo -n "Running G4 simulation"
exec ${g4_exec} -m ${g4_macro} 1>/dev/null 2>/dev/null &

# Wait until the simulation finishes (no child processes)
echo -n " ... waiting "
while [ $(pgrep -c -P$$) -ne 0 ]; do
    sleep 0.5
done
echo " ... done"

# Remove space from energy string
energy=$(echo $energy | tr -d ' ')

# Save output file
mv out.root "${out_dir}/g4_${particle}_${energy}_${events}evt_ext.root"
