# fwd-muon

Geant4 simulation of the LHCf forward calorimeter with the addition of a custom detector to explore its suitability for outgoing muon measurements.
