#include "StackingAction.hh"
#include "G4AnalysisManager.hh"
#include "G4VProcess.hh"

#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4Track.hh"
#include "G4ios.hh"
#include "G4RunManager.hh"


G4ClassificationOfNewTrack StackingAction::ClassifyNewTrack(const G4Track * aTrack) {
    auto analysisManager = G4AnalysisManager::Instance();

    // Check if new track is muon (PDGID=13)
    if(std::abs(aTrack->GetParticleDefinition()->GetPDGEncoding()) == 13) {
        // Check if it was created in the calorimeter
        auto vol_name = aTrack->GetVolume()->GetName();
        if (vol_name == "Abs" || vol_name == "Scint" || vol_name == "Det" || vol_name == "AbsExt") {
            auto eventID = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
            auto pos = aTrack->GetPosition();
            auto momentum = aTrack->GetMomentum();

            analysisManager->FillNtupleIColumn(2, 0, eventID);
            analysisManager->FillNtupleDColumn(2, 1, pos.x());
            analysisManager->FillNtupleDColumn(2, 2, pos.y());
            analysisManager->FillNtupleDColumn(2, 3, pos.z());
            analysisManager->FillNtupleDColumn(2, 4, momentum.x());
            analysisManager->FillNtupleDColumn(2, 5, momentum.y());
            analysisManager->FillNtupleDColumn(2, 6, momentum.z());
            analysisManager->FillNtupleDColumn(2, 7, aTrack->GetKineticEnergy());
            analysisManager->AddNtupleRow(2);
        }
    }

    return fUrgent;
}
