#include "DetectorConstruction.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Cons.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Trd.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4AutoDelete.hh"
#include "G4PVDivision.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4SDManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include <cmath>

DetectorConstruction::DetectorConstruction() {
    // Build order and optional extension
    // A - absorber (W)
    // S - scintillator (G4_Glass_Plate)
    // D - detector (Si)
    std::string base_order = "ASASADDSASASADDSASASADSASASADASAASADASAASADASA";
    std::string order = base_order;

    unsigned int extension = 10;

    for (unsigned int i = 0; i < extension; i++) {
        order.append("E");
    }

    build_order = order;
}

G4VPhysicalVolume* DetectorConstruction::Construct() {
    return DefineVolumes();
}

G4VPhysicalVolume* DetectorConstruction::DefineVolumes() {
    // Materials definition
    auto nistManager = G4NistManager::Instance();
    auto mat_world = nistManager->FindOrBuildMaterial("G4_AIR");
    auto mat_abs = nistManager->FindOrBuildMaterial("G4_W");
    auto mat_scint = nistManager->FindOrBuildMaterial("G4_GLASS_PLATE");
    auto mat_det = nistManager->FindOrBuildMaterial("G4_Si");
    auto vacuum = nistManager->FindOrBuildMaterial("G4_Galactic");

    // Layer dimensions
    auto layer_X = 0.5 * 32.0*mm;
    auto layer_Y = 0.5 * 32.0*mm;

    thickness_map['A'] = 7.5*mm;
    thickness_map['S'] = 2.5*mm;
    thickness_map['D'] = 1.25*mm;
    thickness_map['E'] = 2.5*cm;

    // Create world
    auto world_S = new G4Box("World", 1.*m, 1.*m, 1.*m);
    auto world_LV = new G4LogicalVolume(world_S, mat_world, "World");
    auto world_PV = new G4PVPlacement(0, G4ThreeVector(), world_LV, "World", 0, 0, 0);

    // Create solids and logical volumes
    // Absorber
    auto abs_S = new G4Box("Abs", layer_X, layer_Y, thickness_map.at('A')/2);
    auto abs_LV = new G4LogicalVolume(abs_S, mat_abs, "Abs");
    // Scintillator - union of two solids
    auto height = 0.5 * 14*mm;
    auto taper = 0.6;
    auto scint_S = new G4Box("Scint", layer_X, layer_Y, thickness_map.at('S')/2);
    auto scint_ext_S = new G4Trd("Scint_ext", layer_X, layer_X*taper, thickness_map.at('S')/2, thickness_map.at('S')/2, height);
    auto rot = new G4RotationMatrix();
    rot->rotateX(90*deg);
    auto scint_unionS = new G4UnionSolid("Scint",scint_S, scint_ext_S,  rot, G4ThreeVector(0, layer_Y + height, 0));
    auto scint_LV = new G4LogicalVolume(scint_unionS, mat_scint, "Scint");
    // Silicon detector
    auto det_S = new G4Box("Det", layer_X, layer_Y, thickness_map.at('D')/2);
    auto det_LV = new G4LogicalVolume(det_S, mat_det, "Det");
    // Absorber extension
    auto ext_S = new G4Box("AbsExt", layer_X, layer_Y, thickness_map.at('E')/2);
    auto ext_LV = new G4LogicalVolume(ext_S, mat_abs, "AbsExt");

    // Map for easier access to logical volumes
    LV_map['A'] = abs_LV;
    LV_map['S'] = scint_LV;
    LV_map['D'] = det_LV;
    LV_map['E'] = ext_LV;

    // Create physical volumes based on defined order
    auto current_Z = 0.0;
    auto vol_id = vol_init_id;
    for (const auto& layer : build_order) {
        if (layer == 'E') continue;

        // Get logical volume and thickness
        auto log_vol = LV_map.at(layer);
        auto thick = thickness_map.at(layer);

        // Place physical volume
        auto PV = new G4PVPlacement(0, G4ThreeVector(0, 0, current_Z + thick/2), log_vol, log_vol->GetName(), world_LV, 1, vol_id, 0);
        volumes.push_back(PV);

        // Update current Z position and volume ID
        current_Z += thick;
        vol_id++;
    }

    // Offset the extension from the base calorimeter
    current_Z += 0*cm;

    // Build calorimeter extension if requested
    for (const auto& layer : build_order) {
        if (layer != 'E') continue;

        // Place physical volume
        auto PV = new G4PVPlacement(0, G4ThreeVector(0, 0, current_Z + thickness_map.at('E')/2), ext_LV, "AbsExt", world_LV, 1, vol_id, 0);
        volumes.push_back(PV);

        // Update current Z position and volume ID
        current_Z += thickness_map.at('E');
        vol_id++;
    }

    auto tracker_offset = 2.0*cm;
    auto tracker_S = new G4Box("Tracker", layer_X, layer_Y, 1*nm);
    auto tracker_LV = new G4LogicalVolume(tracker_S, vacuum, "Tracker");
    auto tracker_PV = new G4PVPlacement(0, G4ThreeVector(0, 0, current_Z + tracker_offset), tracker_LV, "Tracker", world_LV, 0, 1, 0);

    world_LV->SetVisAttributes(G4VisAttributes::GetInvisible());

    auto simpleBoxVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,1.0));
    simpleBoxVisAtt->SetVisibility(true);

    return world_PV;
}

void DetectorConstruction::ConstructSDandField() {
    TrackerSD* tracker = new TrackerSD("Tracker", "Tracker Collection");
    G4SDManager::GetSDMpointer()->AddNewDetector(tracker);
    SetSensitiveDetector("Tracker", tracker, false);
}
