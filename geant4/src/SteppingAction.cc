#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"
#include "RunAction.hh"
#include "G4Step.hh"
#include "G4RunManager.hh"
#include "G4AnalysisManager.hh"

SteppingAction::SteppingAction(EventAction* eventAction)
    :   G4UserSteppingAction(),
        fEventAction(eventAction),
        fEdep(eventAction->get_edep_map()),
        fMulti(eventAction->get_multi_map())  {

    G4cout << "[SteppingAction] Initializing stepping action linked to a map of size " << fEdep->size() << G4endl;
    auto layers = eventAction->get_run_action()->get_first_last_layer();
    fLayerFirst = layers.first;
    fLayerLast = layers.second;
}

void SteppingAction::UserSteppingAction(const G4Step* step) {
    if (first_step) {
        fEventAction->set_primary_energy(step->GetPreStepPoint()->GetKineticEnergy());
        fEventAction->set_primary_type(step->GetTrack()->GetParticleDefinition()->GetParticleName());
        first_step = 0;
    }
    auto analysisManager = G4AnalysisManager::Instance();
    auto volume = step->GetPreStepPoint()->GetTouchableHandle()->GetVolume();
    auto volume_past = step->GetPostStepPoint()->GetTouchableHandle()->GetVolume();
    auto pdgid = step->GetTrack()->GetParticleDefinition()->GetPDGEncoding();

    auto vol_id = volume->GetCopyNo();

    if (fLayerFirst <= vol_id && vol_id <= fLayerLast) {
        auto step_edep = step->GetTotalEnergyDeposit();
        fEdep->at(vol_id) += step_edep;
    }
}
