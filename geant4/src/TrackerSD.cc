#include "TrackerSD.hh"
#include "TrackerHit.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4MTRunManager.hh"
#include "G4RunManager.hh"
#include "G4AnalysisManager.hh"


TrackerSD::TrackerSD(const G4String& name, const G4String& hitsCollectionName)
    : G4VSensitiveDetector(name),
      fHitsCollection(NULL) {
      collectionName.insert(hitsCollectionName);
}

void TrackerSD::Initialize(G4HCofThisEvent* hce) {
    fHitsCollection = new TrackerHitsCollection(SensitiveDetectorName, collectionName[0]);

    G4int hcID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    hce->AddHitsCollection( hcID, fHitsCollection );
}

G4bool TrackerSD::ProcessHits(G4Step* aStep, G4TouchableHistory* history) {
    // Skip if particle isn't charged
    if (aStep->GetTrack()->GetParticleDefinition()->GetPDGCharge() == 0)
      return false;

    // Calculate the hit angle
    auto phi = 0.0;
    auto vec = aStep->GetTrack()->GetMomentumDirection();
    phi = vec.angle(G4ThreeVector(0,0,1));

    // Create a new hit object
    auto newHit = new TrackerHit(aStep->GetTrack()->GetTrackID(),
                                 aStep->GetPostStepPoint()->GetPosition(),
                                 aStep->GetTrack()->GetMomentumDirection().angle(G4ThreeVector(0,0,1))/CLHEP::degree,
                                 aStep->GetTrack()->GetTotalEnergy(),
                                 aStep->GetTrack()->GetKineticEnergy(),
                                 aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding(),
                                 aStep->GetTrack()->GetParticleDefinition()->GetPDGCharge(),
                                 aStep->GetTrack()->GetGlobalTime());

    fHitsCollection->insert(newHit);

    return true;
}

void TrackerSD::EndOfEvent(G4HCofThisEvent*) {
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    G4int eventID = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();

    G4int nofHits = fHitsCollection->entries();
    if (nofHits!=0) {
        for (G4int i = nofHits-1; i >= 0; i--) {
            man->FillNtupleIColumn(1, 0, eventID);
            man->FillNtupleIColumn(1, 1, (*fHitsCollection)[i]->GetTrackID());
            man->FillNtupleDColumn(1, 2, (*fHitsCollection)[i]->GetX());
            man->FillNtupleDColumn(1, 3, (*fHitsCollection)[i]->GetY());
            man->FillNtupleDColumn(1, 4, (*fHitsCollection)[i]->GetPhi());
            man->FillNtupleDColumn(1, 5, (*fHitsCollection)[i]->GetKineticEnergy());
            man->FillNtupleIColumn(1, 6, (*fHitsCollection)[i]->GetPDGID());
            man->FillNtupleIColumn(1, 7, (*fHitsCollection)[i]->IsCharged());
            man->FillNtupleDColumn(1, 8, (*fHitsCollection)[i]->GetGlobalTime());
            man->AddNtupleRow(1);
        }
    }
}
