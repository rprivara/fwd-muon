#include "EventAction.hh"
#include "RunAction.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4UnitsTable.hh"
#include "G4AnalysisManager.hh"

#include "Randomize.hh"
#include <iomanip>


EventAction::EventAction(RunAction* runAction)
    : fRunAction(runAction) {

    // Get layer volume ID from run action
    auto layers = get_run_action()->get_first_last_layer();
    fLayerFirst = layers.first;
    fLayerLast = layers.second;

    // Initialize deposited energy map
    for (unsigned int l = fLayerFirst; l <= fLayerLast; l++) {
        fEdep[l] = 0.0;
        fMulti[l] = 0;
    }
    G4cout << "[EventAction] Initialized edep map of " << fEdep.size() << " layers (from " << fLayerFirst << " to " << fLayerLast << ")." << G4endl;
}


void EventAction::BeginOfEventAction(const G4Event* event) {
    for ( auto& edep : fEdep ) {
        edep.second = 0.;
    }
    for ( auto& multi : fMulti ) {
        multi.second = 0.;
    }
}

void EventAction::EndOfEventAction(const G4Event* event) {
    auto analysisManager = G4AnalysisManager::Instance();

    auto contained_energy = 0.0;
    for (auto& edep : fEdep) {
        analysisManager->FillH1(0, edep.first - fLayerFirst, edep.second);
        analysisManager->FillNtupleDColumn(0, edep.first - fLayerFirst, edep.second);

        contained_energy += edep.second;
    }
    analysisManager->FillH1(1, contained_energy / primary_energy * 100);

    analysisManager->AddNtupleRow(0);
}
