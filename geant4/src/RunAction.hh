#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "DetectorConstruction.hh"
#include <array>

class G4Run;

class RunAction : public G4UserRunAction {
    public:
        RunAction(DetectorConstruction* detConst);

        virtual ~RunAction();

        virtual void BeginOfRunAction(const G4Run*);
        virtual void   EndOfRunAction(const G4Run*);

        std::pair<unsigned int, unsigned int> get_first_last_layer() { return fDetConst->get_first_last_layer(); }

    private:
        DetectorConstruction* fDetConst;
};

#endif
