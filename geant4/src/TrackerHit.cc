#include "TrackerHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include <iomanip>

G4ThreadLocal G4Allocator<TrackerHit>* TrackerHitAllocator=0;

TrackerHit::TrackerHit(G4int trackID, G4ThreeVector pos, G4double phi, G4double energy, G4double kinetic, G4int PDGID, G4bool charged, G4double globalTime)
    : G4VHit(),
      fTrackID(trackID),
      fPos(pos),
      fPhi(phi),
      fEnergy(energy),
      fKinetic(kinetic),
      fPDGID(PDGID),
      fCharged(charged),
      fGlobalTime(globalTime) {}


// TrackerHit::TrackerHit(const TrackerHit& right)
//     : G4VHit() {
//     fTrackID   = right.fTrackID;
//     fEdep      = right.fEdep;
//     fPos       = right.fPos;
//     fGlobalTime = right.fGlobalTime;
// }

// const TrackerHit& TrackerHit::operator=(const TrackerHit& right) {
//     fTrackID   = right.fTrackID;
//     fEdep      = right.fEdep;
//     fPos       = right.fPos;
//     fGlobalTime = right.fGlobalTime;

//     return *this;
// }

// G4int TrackerHit::operator==(const TrackerHit& right) const {
//     return ( this == &right ) ? 1 : 0;
// }

void TrackerHit::Print() {
    G4cout << "Hit " << fTrackID << ":" << G4endl
           << "  - position       = " <<  fPos << G4endl
           << "  - phi            = " <<  G4BestUnit(fPhi,"Angle") << G4endl
           << "  - total energy   = " <<  G4BestUnit(fEnergy,"Energy") << G4endl
           << "  - kinetic energy = " <<  G4BestUnit(fKinetic,"Energy") << G4endl
           << "  - PDGID          = " <<  fPDGID << G4endl
           << "  - charged        = " <<  fCharged << G4endl
           << "  - time           = " <<  G4BestUnit(fGlobalTime,"Time") << G4endl;
}
