#include "ActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"
#include "StackingAction.hh"
#include "DetectorConstruction.hh"


ActionInitialization::ActionInitialization (DetectorConstruction* detConstruction)
    : G4VUserActionInitialization(),
    fDetConstruction(detConstruction) {}

void ActionInitialization::BuildForMaster() const {
    SetUserAction(new RunAction(fDetConstruction));
}

void ActionInitialization::Build() const {
    SetUserAction(new PrimaryGeneratorAction);

    RunAction* fRunAction = new RunAction(fDetConstruction);
    SetUserAction(fRunAction);

    auto eventAction = new EventAction(fRunAction);
    SetUserAction(eventAction);
    SetUserAction(new SteppingAction(eventAction));
    SetUserAction(new StackingAction());
}
