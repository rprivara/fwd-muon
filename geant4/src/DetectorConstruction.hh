#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include <array>
#include <numeric>
#include "TrackerSD.hh"

class G4VPhysicalVolume;
class G4GlobalMagFieldMessenger;


class DetectorConstruction : public G4VUserDetectorConstruction {
    public:
        DetectorConstruction();
        virtual ~DetectorConstruction() {};
        virtual G4VPhysicalVolume* Construct();
        virtual void ConstructSDandField();

        inline void set_build_order(std::string& order) { build_order = order; }
        inline std::string get_build_order() { return build_order; }

        inline unsigned int get_n_layers() { return build_order.size(); }
        inline std::pair<unsigned int, unsigned int> get_first_last_layer() {
            return std::make_pair(vol_init_id, vol_init_id + get_n_layers() - 1);
        }

    private:
        G4VPhysicalVolume* DefineVolumes();
        std::vector<G4VPhysicalVolume*> volumes{};

        std::map<char, G4LogicalVolume*> LV_map;
        std::map<char, double> thickness_map;
        unsigned int vol_init_id = 10;

        std::string build_order{};
};

#endif
