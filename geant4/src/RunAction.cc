#include "RunAction.hh"

#include <string>
#include <iostream>
#include <fstream>

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "DetectorConstruction.hh"
#include "G4EmCalculator.hh"
#include "G4AnalysisManager.hh"


RunAction::RunAction(DetectorConstruction* detConst)
    : G4UserRunAction(),
    fDetConst(detConst) {

    G4RunManager::GetRunManager()->SetPrintProgress(1);
    auto analysisManager = G4AnalysisManager::Instance();

    // Get the layer vector and the total number of layers
    auto [layer_first, layer_last] = get_first_last_layer();
    auto n_layers = fDetConst->get_n_layers();

    analysisManager->CreateH1("Deposited_energy", "Total deposited energy", n_layers, -0.5, n_layers-0.5);
    analysisManager->SetH1XAxisTitle(0, "Layer");
    analysisManager->SetH1YAxisTitle(0, "Deposited energy [MeV]");

    analysisManager->CreateH1("Containment", "Containment", 100, 0, 100);
    analysisManager->SetH1XAxisTitle(1, "Containment [%]");
    analysisManager->SetH1YAxisTitle(1, "Events");

    analysisManager->CreateNtuple("Layers", "Layers");
    auto layers = fDetConst->get_build_order();
    for (unsigned int l = 0; l < n_layers; l++) {
        G4String layer_type;
        if (layers.at(l) == 'A') layer_type = "Abs";
        if (layers.at(l) == 'S') layer_type = "Scint";
        if (layers.at(l) == 'D') layer_type = "Det";
        if (layers.at(l) == 'E') layer_type = "Ext";

        G4String column_name = "L" + std::to_string(l) + "_" + layer_type;
        analysisManager->CreateNtupleDColumn(column_name);
    }
    analysisManager->FinishNtuple();

    analysisManager->CreateNtuple("Tracker", "Tracker");
    analysisManager->CreateNtupleIColumn("eventID");
    analysisManager->CreateNtupleIColumn("trackID");
    analysisManager->CreateNtupleDColumn("X");
    analysisManager->CreateNtupleDColumn("Y");
    analysisManager->CreateNtupleDColumn("Phi");
    analysisManager->CreateNtupleDColumn("KineticEnergy");
    analysisManager->CreateNtupleIColumn("PDGID");
    analysisManager->CreateNtupleIColumn("isCharged");
    analysisManager->CreateNtupleDColumn("globalTime");
    analysisManager->FinishNtuple();

    analysisManager->CreateNtuple("Muons", "Muons");
    analysisManager->CreateNtupleIColumn("eventID");
    analysisManager->CreateNtupleDColumn("X");
    analysisManager->CreateNtupleDColumn("Y");
    analysisManager->CreateNtupleDColumn("Z");
    analysisManager->CreateNtupleDColumn("MomentumX");
    analysisManager->CreateNtupleDColumn("MomentumY");
    analysisManager->CreateNtupleDColumn("MomentumZ");
    analysisManager->CreateNtupleDColumn("KineticEnergy");
    analysisManager->FinishNtuple();
}

RunAction::~RunAction() {
    delete G4AnalysisManager::Instance();
}

void RunAction::BeginOfRunAction(const G4Run*) {
    auto analysisManager = G4AnalysisManager::Instance();
    analysisManager->SetNtupleMerging(true);

    std::string file_name = "out.root";

    analysisManager->OpenFile(file_name);
}

void RunAction::EndOfRunAction(const G4Run*) {
    auto analysisManager = G4AnalysisManager::Instance();
    analysisManager->Write();
    analysisManager->CloseFile();
}
