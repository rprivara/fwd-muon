#ifndef SteppingAction_h
#define SteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include <array>
#include "DetectorConstruction.hh"

class DetectorConstruction;
class EventAction;

class SteppingAction : public G4UserSteppingAction {
    public:
        SteppingAction(EventAction* eventAction) ;
        virtual ~SteppingAction() {};

        virtual void UserSteppingAction(const G4Step* step);
    private:
        EventAction*  fEventAction;
        std::map<G4int, G4double> *fEdep;
        std::map<G4int, G4int> *fMulti;
        unsigned int fLayerFirst, fLayerLast;
        bool first_step = 1;
        double init_energy;
};

#endif
