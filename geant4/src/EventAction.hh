#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "DetectorConstruction.hh"
#include "globals.hh"
#include "RunAction.hh"

#include <array>

class EventAction : public G4UserEventAction {
    public:
        EventAction(RunAction* runAction);
        virtual ~EventAction() {};

        virtual void  BeginOfEventAction(const G4Event* event);
        virtual void    EndOfEventAction(const G4Event* event);

        inline std::map<G4int, G4double>* get_edep_map() { return &fEdep; }
        inline std::map<G4int, G4int>* get_multi_map() { return &fMulti; }

        inline RunAction* get_run_action() { return fRunAction; }

        void set_primary_energy (double energy) { primary_energy = energy; }
        void set_primary_type (G4String type) { primary_type = type; }

    private:
        RunAction* fRunAction;
        std::map<G4int, G4double> fEdep;
        std::map<G4int, G4int> fMulti;
        unsigned int fLayerFirst, fLayerLast;
        double primary_energy{};
        G4String primary_type{};
};

#endif
