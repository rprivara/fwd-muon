#ifndef TrackerHit_h
#define TrackerHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "tls.hh"

class TrackerHit : public G4VHit {
    public:
        // Default ctor
        TrackerHit() {};
        // Ctor with full members
        TrackerHit(G4int trackID, G4ThreeVector pos, G4double phi, G4double energy, G4double kinetic, G4int PDGID, G4bool charged, G4double globalTime);
        // TrackerHit(const TrackerHit&);

        // Default dtor
        virtual ~TrackerHit() {};

        // operators
        // const TrackerHit& operator=(const TrackerHit&);
        // G4int operator==(const TrackerHit&) const;

        inline void* operator new(size_t);
        inline void  operator delete(void*);

        // Print information about the hit
        virtual void Print();

        // Set methods
        inline void SetTrackID (G4int trackID) { fTrackID = trackID; }
        inline void SetPos (G4ThreeVector pos) { fPos = pos; }
        inline void SetPhi (G4double phi) { fPhi = phi; }
        inline void SetTotalEnergy (G4double energy) { fEnergy = energy; }
        inline void SetKineticEnergy (G4double kinetic) { fKinetic = kinetic; }
        inline void SetPDGID (G4int PDGID) { fPDGID = PDGID; }
        inline void SetCharged (G4bool charged) { fCharged = charged; }
        inline void SetGlobalTime (G4double globalTime){ fGlobalTime = globalTime;}

        // Get methods
        inline G4int GetTrackID() const { return fTrackID; }
        inline G4ThreeVector GetPos() const { return fPos; }
        inline G4double GetX() const { return fPos.x(); }
        inline G4double GetY() const { return fPos.y(); }
        inline G4double GetPhi() const { return fPhi; }
        inline G4double GetTotalEnergy() const { return fEnergy; }
        inline G4double GetKineticEnergy() const { return fKinetic; }
        inline G4int GetPDGID() const { return fPDGID; }
        inline G4bool IsCharged() const { return fCharged; }
        inline G4double GetGlobalTime() const { return fGlobalTime; }
    private:
        G4int fTrackID{};
        G4ThreeVector fPos{};
        G4double fPhi{};
        G4double fEnergy{};
        G4double fKinetic{};
        G4int fPDGID{};
        G4bool fCharged{};
        G4double fGlobalTime{};
};

typedef G4THitsCollection<TrackerHit> TrackerHitsCollection;

extern G4ThreadLocal G4Allocator<TrackerHit>* TrackerHitAllocator;

inline void* TrackerHit::operator new(size_t) {
    if(!TrackerHitAllocator)
        TrackerHitAllocator = new G4Allocator<TrackerHit>;
    return (void *) TrackerHitAllocator->MallocSingle();
}

inline void TrackerHit::operator delete(void *hit) {
    TrackerHitAllocator->FreeSingle((TrackerHit*) hit);
}

#endif
